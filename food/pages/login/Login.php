
<?php
$link = mysqli_connect("localhost", "root", "", "orderfoodonline");

// Check connection
if($link === false){

    die("ERROR: Could not connect. " . mysqli_connect_error());

}
 
// Define variables and initialize with empty values
$username = $password = $credit = $familyname = "";
$username_err = $password_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    // Check if username is empty
    if(empty(trim($_POST["username"]))){
        $username_err = 'Please enter username.';
    } else{
        $username = trim($_POST["username"]);
    }
    
    // Check if password is empty
    if(empty(trim($_POST['pass']))){
        $password_err = 'Please enter your password.';
    } else{
        $password = trim($_POST['pass']);
    }
    
    // Validate credentials
    if(empty($username_err) && empty($password_err)){
        // Prepare a select statement
        $sql = "SELECT username, pass FROM person WHERE username = ?";
        
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            
            // Set parameters
            $param_username = $username;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Store result
                mysqli_stmt_store_result($stmt);
                
                // Check if username exists, if yes then verify password
                if(mysqli_stmt_num_rows($stmt) == 1){                    
                    // Bind result variables
                    mysqli_stmt_bind_result($stmt, $username, $hashed_password);
                    $h_p = password_hash($password, PASSWORD_DEFAULT);
                    if(mysqli_stmt_fetch($stmt)){
                     
                        if(password_verify($hashed_password, $h_p)){
                            /* Password is correct, so start a new session and
                            save the username to the session */
                            session_start();
                            $_SESSION['username'] = $username; 
              
                            $sql = "SELECT familyname , credit , id FROM Person where username = '$username' and pass='$password'";
            
                            if($result = mysqli_query($link, $sql))
                            {  
                                if (mysqli_num_rows($result) > 0) {
                                        // output data of each row
                                        $row = mysqli_fetch_assoc($result);
                                        $_SESSION['credit'] =  $row["credit"];   
                                        $_SESSION['familyname'] = $row["familyname"];   
                                        $_SESSION['id'] = $row["id"];    
                                
                                } else {
                                    echo "0 results";
                                }
                            }
                      
                            header("location: ../../");
                        } else{
                            // Display an error message if password is not valid   
                             echo $h_p;
                            $password_err = 'The password you entered was not valid.';
                        }  
                    }
                } else{
                    // Display an error message if username doesn't exist
                    $username_err = 'No account found with that username.';
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
        
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    // Close connection
    mysqli_close($link);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  
    <meta charset="UTF-8">
    <title>ATIIDOR</title>

    <!--STYLESHEETS SHOULD BE LOADED HERE :)-->
<link rel="stylesheet" href="../../css/atiyehnorouzzadeh.css">
    <link rel="stylesheet" href="../../css/dorsasamiee.css">

    <link rel="stylesheet" href="../../css/ui-bootstrap-csp.css">
    <link rel="stylesheet"  href="../../css/font-awesome.css" />
    <link rel="stylesheet"  href="../../css/animate.css">
    <link rel="stylesheet"  href="../../css/responsive.css" />
    <!--STYLESHEETS FROM BOWER-->
</head>
 <body style="position: relative">

<div class="container-fluid index">
    <div class="main col-md-4 col-sm-12  col-md-offset-4">
        <form class="register-form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method = "POST" style="margin-top : 10%;">

            <div class="  col-md-12 col-sm-12 col-xs-12 ">
                <div class="col-md-2 col-sm-12 col-xs-12 mylabel margin">
                    <label>Username</label>
                     <span class="help-block"><?php echo $username_err; ?></span>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12 ">
                    <input type="text" placeholder="Enter your Username" class="form-control" name = "username">
                </div>
            </div>
            <div class=" col-md-12 col-sm-12 col-xs-12 ">
                <div class="col-md-2 col-sm-12 col-xs-12 mylabel margin">
                    <label>Password</label>
                    <span class="help-block"><?php echo $password_err; ?></span>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12 ">
                    <input type="password" placeholder="Enter your Password" class="form-control" name = "pass">
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 field">
        
                <div class="col-md-12 col-sm-12 col-xs-12 field " style="margin-top:15px ">
                    <div class=" col-md-2 col-sm-6 col-xs-8 button1 ">
                        <button type="submit" name="submit" value="login" class="btn btn-default btn-md ">Login</button>
                    </div>
                </div>

            </div>
            <p>Don't have an account? <a href="register.php">Sign up now</a>.</p
        </form>
    </div>
</div>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAK3nwDhmzXhT_7p5LP-s9JsSMWSDz9xYk&c&libraries=places&sensor=false"
        type="text/javascript"></script>

</body>