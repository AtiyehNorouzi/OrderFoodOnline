<?php
session_start();
$restaurantNumbers =0;
$link = mysqli_connect("localhost", "root", "", "orderfoodonline");

// Check connection
if($link === false){

    die("ERROR: Could not connect. " . mysqli_connect_error());
}
if(isset($_SESSION['rest'])  && !empty($_SESSION['rest']))
{
    $rest = $_SESSION["rest"];
    $bd = "SELECT * FROM restaurant WHERE fpname = '$rest' ";

    if($result =  mysqli_query($link, $bd)){
        error_reporting(E_ALL); ini_set('display_errors', 'On'); 
      

            if (mysqli_num_rows($result) > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) 
                    {
                      //retaurant info
                        $restaurantNumbers += 1;
                    }

            
            } else {
                echo "0 results";
            }

} else{
  echo "ERROR: Could not able to execute $bd. " . mysqli_error($link);
}

}
else if(isset($_SESSION['address'])  && !empty($_SESSION['address']))
{
    $add = $_SESSION["address"];
    $bd = "SELECT * FROM restaurant WHERE address = '$add' ";
    if($result =  mysqli_query($link, $bd)){
        error_reporting(E_ALL); ini_set('display_errors', 'On'); 
      

            if (mysqli_num_rows($result) > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) 
                    {
                      //restaurant info
                      $restaurantNumbers += 1;
                    }
            
            } else {
                echo "0 results";
            }

} else{
  echo "ERROR: Could not able to execute $bd. " . mysqli_error($link);
}

}
else
{
        //load all
        $bd = "SELECT * FROM restaurant";

        if($result = mysqli_prepare($link, $bd))
        {
        
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($result)){
                // Store result
                mysqli_stmt_store_result($result);
                
                if(mysqli_stmt_num_rows($result) == 1){                    
                    if(mysqli_stmt_fetch($result)){
        
                            if($result = mysqli_query($link, $bd))
                            {  
                                if (mysqli_num_rows($result) > 0) {
                                        // output data of each row
                                        $restaurantNumbers = $restaurantNumbers + 1;   
                                
                                } else {
                                    echo "0 results";
                                }
                            }
                        }
                    }
                } 
            else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }    
}


        
    //     // Close statement
    //     mysqli_stmt_close($bd);
    
    // // Close connection
    // mysqli_close($link);



?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Colorlib">
    <meta name="description" content="#">
    <meta name="keywords" content="#">
    <!-- Favicons -->
    <link rel="shortcut icon" href="#">
    <!-- Page Title -->
    <title>Listing &amp; Directory Website Template</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700,900" rel="stylesheet">
    <!-- Simple line Icon -->
    <link rel="stylesheet" href="css/simple-line-icons.css">
    <!-- Themify Icon -->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- Hover Effects -->
    <link rel="stylesheet" href="css/set1.css">
    <!-- Main CSS -->
    <link rel="stylesheet" href="css/liststyle.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>

    <!--//END HEADER -->
    <!--============================= DETAIL =============================-->
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-7 responsive-wrap">
                    <div class="row detail-filter-wrap">
                        <div class="col-md-4 featured-responsive">
                            <div class="detail-filter-text">
                                <p><?php echo $restaurantNumbers ?>
                                    <span>Restaurant</span>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-8 featured-responsive">
                            <div class="detail-filter">
                                <p>Filter by</p>
                                <form class="filter-dropdown">
                                    <select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="inlineFormCustomSelect">
                                        <option selected>Best Match</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </form>
                                <form class="filter-dropdown">
                                    <select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="inlineFormCustomSelect1">
                                        <option selected>Restaurants</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </form>
                                <div class="map-responsive-wrap">
                                    <a class="map-icon" href="#">
                                        <span class="icon-location-pin"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row detail-checkbox-wrap">
                        <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">

                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Bike Parking</span>
                            </label>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Wireless Internet </span>
                            </label>
                        </div>

                        <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">

                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Smoking Allowed </span>
                            </label>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Street Parking</span>
                            </label>
                        </div>

                        <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">

                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Special</span>
                            </label>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Accepts Credit cards</span>
                            </label>
                        </div>

                        <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">

                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Pets Friendly</span>
                            </label>

                        </div>
                    </div>
                    <div class="row light-bg detail-options-wrap">
                        <?php 
                            if($result =  mysqli_query($link, $bd)){
                                error_reporting(E_ALL); ini_set('display_errors', 'On'); 
                              
                        
                                    if (mysqli_num_rows($result) > 0) {
                                            // output data of each row
                                            while($row = $result->fetch_assoc()) 
                                            {
                                              //retaurant info
                                              ?>
                                                             <div class="col-sm-6 col-lg-12 col-xl-6 featured-responsive">
                                                                <div class="featured-place-wrap">
                                                                    <a href="detail.html">
                                                                        <img src="images/featured3.jpg" class="img-fluid" alt="#">
                                                                        <span class=<?php if($row["rate"] <= 4.5 ){ ?>
                                                                        "featured-rating" <?php }
                                                                            else if($row["rate"] < 8){?>
                                                                                "featured-rating-orange"
                                                                            <?php }else{?>
                                                                                "featured-rating-green"
                                                                            <?php } ?>       ><?php echo $row["rate"] ?></span>
                                                                        <div class="featured-title-box">
                                                                            <h6><?php echo $row["fpname"]; ?></h6>
                                                                            <p>Restaurant </p>
                                                                            <span>• </span>
                                                                            <p>3 Reviews</p>
                                                                            <span> • </span>
                                                                            <p>
                                                                                <span>$$$</span>$$</p>
                                                                            <ul>
                                                                                <li>
                                                                                    <span class="icon-location-pin"></span>
                                                                                    <p><?php echo $row["address"] ?></p>
                                                                                </li>
                                                                                <li>
                                                                                    <span class="icon-screen-smartphone"></span>
                                                                                    <p><?php echo $row["phonenumber"] ?></p>
                                                                                </li>
                                                                                <li>
                                                                                    <span class="icon-link"></span>
                                                                                    <p>https://burgerandlobster.com</p>
                                                                                </li>

                                                                            </ul>
                                                                            <div class="bottom-icons">
                                                                            <?php
                                                                                    $timezone = date_default_timezone_set('Asia/Tehran');
                                                                                    if(date('h:i a', time()) > date('h:i a', strtotime($row["timetosendOrder"])) ){
                                                                                    ?> <div class="closed-now">CLOSED NOW</div>  OPEN AT : 
                                                                                    <?php  echo date('h:i a', strtotime($row["timetosendOrder"])); }
                                                                                    else{ ?> <div class="open-now">OPEN NOW</div> <?php } 
                                                                            ?>

                                                                                <span class="ti-heart"></span>
                                                                                <span class="ti-bookmark"></span>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            </div>


                                              <?php
                                       
                                            }
                        
                                    
                                    } else {
                                        echo "0 results";
                                    }
                        
                        } else{
                          echo "ERROR: Could not able to execute $bd. " . mysqli_error($link);
                        }
    
                        ?>

                 
                    </div>
                </div>
                <div class="col-md-5 responsive-wrap map-wrap">
                    <div class="map-fix">
                        <!-- data-toggle="affix" -->
                        <!-- Google map will appear here! Edit the Latitude, Longitude and Zoom Level below using data-attr-*  -->
                        <div id="map" data-lat="40.674" data-lon="-73.945" data-zoom="14"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--//END DETAIL -->
    <!--============================= FOOTER =============================-->
    <footer class="main-block dark-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        <p>Copyright &copy; 2018 . All rights reserved | This template is made with
                            <i class="glyphicon glyphicon-heart"></i> by Atiidor
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--//END FOOTER -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>


    <script>
        $(".map-icon").click(function () {
            $(".map-fix").toggle();
        });
    </script>
    <script>
        // Want to customize colors? go to snazzymaps.com
        function myMap() {
            var maplat = $('#map').data('lat');
            var maplon = $('#map').data('lon');
            var mapzoom = $('#map').data('zoom');
            // Styles a map in night mode.
            var map = new google.maps.Map(document.getElementById('map'), {
                center: {
                    lat: maplat,
                    lng: maplon
                },
                zoom: mapzoom,
                scrollwheel: false
            });
            var marker = new google.maps.Marker({
                position: {
                    lat: maplat,
                    lng: maplon
                },
                map: map,
                title: 'We are here!'
            });
        }
    </script>
    <!-- Map JS (Please change the API key below. Read documentation for more info) -->
    <script src="https://maps.googleapis.com/maps/api/js?callback=myMap&key=AIzaSyDMTUkJAmi1ahsx9uCGSgmcSmqDTBF9ygg"></script>
</body>

</html>